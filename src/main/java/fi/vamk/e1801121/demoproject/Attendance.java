package fi.vamk.e1801121.demoproject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQuery(name = "Attendance.findAll", query = "SELECT p FROM Attendance p")

public class Attendance implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String key;
    private Date day;

    public Attendance() {
    }

    public Attendance(String key, Date day) {

        this.key = key;
        this.day = day;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }


    public String toString() {
        return "ID: " + id + "KEY: " + key + "DAY: "+ day;
    }
}
