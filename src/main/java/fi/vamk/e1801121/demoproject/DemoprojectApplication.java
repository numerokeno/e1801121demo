package fi.vamk.e1801121.demoproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Date;

@SpringBootApplication
public class DemoprojectApplication {

	@Autowired
	private AttendanceRepository attendanceRepository;
	public static void main(String[] args) {
		SpringApplication.run(DemoprojectApplication.class, args);
	}


	@Bean
	public void initDate(){
		long millis=System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		Attendance att = new Attendance("test1", date);
		attendanceRepository.save(att);

		Attendance att2 = new Attendance("test2", date);
		attendanceRepository.save(att2);

		Attendance att3 = new Attendance("test3", date);
		attendanceRepository.save(att3);

	}

}

