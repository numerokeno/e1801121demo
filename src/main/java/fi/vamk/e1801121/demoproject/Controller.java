package fi.vamk.e1801121.demoproject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @RequestMapping("/test")
    public String tests(){
        return "{\"id\":1}";
    }

}
