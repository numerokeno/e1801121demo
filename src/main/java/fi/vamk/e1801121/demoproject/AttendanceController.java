package fi.vamk.e1801121.demoproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AttendanceController {
    @Autowired
    private AttendanceRepository attendanceRepository;

    //GET
    @GetMapping("/attendances")
    public Iterable<Attendance> list(){
       return attendanceRepository.findAll();
    }

    @GetMapping("/attendance/{id}")
    public Optional<Attendance> get(@PathVariable("id") int id){
        return attendanceRepository.findById(id);
    }
    
    //SAVE
    @PostMapping("/attendance")
    public @ResponseBody Attendance create(@RequestBody Attendance item){
        return attendanceRepository.save(item);
    }

    //PUT
    @PutMapping("/attendance")
    public @ResponseBody Attendance update(@RequestBody Attendance item){
        return attendanceRepository.save(item);
    }
    //Delete
    @DeleteMapping("/attendance")
    public void date(@RequestBody Attendance item){
        attendanceRepository.delete(item);
    }

}
